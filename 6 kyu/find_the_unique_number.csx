/*
There is an array with some numbers. All numbers are equal except for one. Try to find it!

findUniq([ 1, 1, 1, 2, 1, 1 ]) === 2
findUniq([ 0, 0, 0.55, 0, 0 ]) === 0.55 ???? INTS
It’s guaranteed that array contains at least 3 numbers.

The tests contain some very huge arrays, so think about performance.

This is the first kata in series:

Find the unique number (this kata)
Find the unique string
Find The Unique
REF: https://www.codewars.com/kata/585d7d5adb20cf33cb000235/train/csharp
*/

using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static int GetUnique(IEnumerable<int> numbers)
    {
        int repeatedNumber, diff, i;
        diff = repeatedNumber = numbers.First();
        for (i = 0; i < numbers.Count() - 1; i++)
        {
            if (numbers.ElementAt(i) == numbers.ElementAt(i + 1))
                repeatedNumber = numbers.ElementAt(i);
            else
                diff = numbers.ElementAt(i);
        }

        if (diff == repeatedNumber)
            diff = numbers.ElementAt(i);
        return diff;
    }
}

Console.WriteLine(Kata.GetUnique(new[] { 1, 1, 1, 2, 1, 1 }));
Console.WriteLine(Kata.GetUnique(new[] { 0, 0, 2, 0, 0 }));
Console.WriteLine(Kata.GetUnique(new[] { 1, 2, 2, 2 }));
Console.WriteLine(Kata.GetUnique(new[] { -2, 2, 2, 2 }));
Console.WriteLine(Kata.GetUnique(new[] { 11, 11, 14, 11, 11 }));
Console.WriteLine(Kata.GetUnique(new[] { 0, 0, 1, 0 }));
Console.WriteLine(Kata.GetUnique(new[] { -3, -3, 0, -3 }));
Console.WriteLine(Kata.GetUnique(new[] { 8, 8, 8, 8, 7 }));