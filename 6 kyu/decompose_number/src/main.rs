// Decompose a number into an array (tuple in Haskell, array of arrays long[][] in C# or Java)
// in the form [ [k1, k2, k3, ...], r ] such that:
// num = 2k1 + 3k2 + 4k3 + ... + nkn-1 + r

// Where every ki > 1 and every ki is maximized (first maximizing for 2, then 3, and so on)
// Examples

// 0   -->  [ [], 0 ]
// 3   -->  [ [], 3 ]        # because there is no `k` more than 1
// 26  -->  [ [4, 2], 1 ]    # 26 = 2^4 + 3^2 + 1
// 8330475  -->  [ [22, 13, 10, 8, 7, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2], 0 ]
//                           # 8330475 = 2^22 + 3^13 + 4^10 + ... + 22^2 + 23^2 + 24^2 + 0
// REF: https://www.codewars.com/kata/55ec80d40d5de30631000025

fn decompose(n: u32) -> (Vec<u32>, u32) {
    if n < 4 {
        return (vec![], n);
    }

    let mut base: u32 = 2;
    let mut remainder: u32 = n;
    let mut results = Vec::<u32>::new();
    while base.pow(2) <= remainder {
        for i in 2..=remainder {
            let temp = base.pow(i);
            if temp > remainder {
                results.push(i - 1);
                remainder -= base.pow(i - 1);
                base += 1;
                break;
            }
            if temp == remainder {
                results.push(i);
                remainder = 0;
                break;
            }
        }
    }
    (results, remainder)
}

fn main() {
    println!("{:?}", decompose(0)); //, (vec![], 0)
    println!("{:?}", decompose(4)); //, (vec![2], 0)
    println!("{:?}", decompose(9)); //, (vec![3], 1)
    println!("{:?}", decompose(25)); //, (vec![4, 2], 0)
    println!("{:?}", decompose(26)); //, (vec![4, 2], 1)
    println!("{:?}", decompose(8330475)); //, (vec![22, 13, 10, 8, 7, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2], 0)
}
