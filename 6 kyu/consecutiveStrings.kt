/*
strarr = ["tree", "foling", "trashy", "blue", "abcdef", "uvwxyz"], k = 2

Concatenate the consecutive strings of strarr by 2, we get:

treefoling   (length 10)  concatenation of strarr[0] and strarr[1]
folingtrashy ("      12)  concatenation of strarr[1] and strarr[2]
trashyblue   ("      10)  concatenation of strarr[2] and strarr[3]
blueabcdef   ("      10)  concatenation of strarr[3] and strarr[4]
abcdefuvwxyz ("      12)  concatenation of strarr[4] and strarr[5]

Two strings are the longest: "folingtrashy" and "abcdefuvwxyz".
The first that came is "folingtrashy" so
longest_consec(strarr, 2) should return "folingtrashy".

In the same way:
longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2) --> "abigailtheta"
REF: https://www.codewars.com/kata/56a5d994ac971f1ac500003e/train/kotlin
*/
fun longestConsec(strarr: Array<String>, k: Int): String {
    val arraySize: Int = strarr.size
    if (arraySize == 0 || k <= 0 || k > arraySize) return ""
    var longestString: String = ""

    for (i in 0..arraySize - 1) {
        var newString: String = ""
        var counter: Int = 0
        while (counter < k && (i + counter < arraySize)) {
            newString += strarr[i + counter]
            counter += 1
        }
        longestString = if (newString.length > longestString.length) newString else longestString
    }

    return longestString
}

fun main() {
    val result = longestConsec(arrayOf<String>("tree", "foling", "trashy", "blue", "abcdef", "uvwxyz"), 2) // folingtrashy
    println("Result1: $result\n")

    val result2 = longestConsec(arrayOf<String>("zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"), 2) // abigailtheta
    println("Result2: $result2\n")

    val result3 = longestConsec(arrayOf<String>("ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"), 1) // oocccffuucccjjjkkkjyyyeehh
    println("Result3: $result3\n")

    val result4 = longestConsec(arrayOf<String>(), 1) // ""
    println("Result4: $result4\n")

    val result5 = longestConsec(arrayOf<String>("ejjjjmmtthh", "zxxuueeg"), 10) // ""
    println("Result5: $result5\n")
}
