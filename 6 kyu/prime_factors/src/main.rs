// Inspired by one of Uncle Bob's TDD Kata

// Write a function that generates factors for a given number.

// The function takes an integer as parameter and returns a list of integers
// (ObjC: array of NSNumbers representing integers).
// That list contains the prime factors in numerical sequence.
// REF: https://www.codewars.com/kata/542f3d5fd002f86efc00081a

fn prime_factors(n: u32) -> Vec<u32> {
    let mut result = Vec::<u32>::new();
    let mut number = n;
    while number % 2 == 0 {
        number = number / 2;
        result.push(2);
    }

    for i in (3..=(number as f64).sqrt() as u32).step_by(2) {
        while number % i == 0 {
            number = number / i;
            result.push(i);
        }
    }

    if number > 2 {
        result.push(number);
    }

    result
}

fn main() {
    println!("{:?}", prime_factors(1)); // &[]
    println!("{:?}", prime_factors(4)); // &[2, 2]
    println!("{:?}", prime_factors(8)); // &[2, 2, 2]
    println!("{:?}", prime_factors(9)); // &[3, 3]
    println!("{:?}", prime_factors(12)); // &[2, 2, 3]
    println!("{:?}", prime_factors(11020332)); // &[2, 2, 3, 918361]
}
