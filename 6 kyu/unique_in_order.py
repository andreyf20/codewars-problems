# Implement the function unique_in_order which takes as argument a sequence and returns a
# list of items without any elements with the same value next to each other and
# preserving the original order of elements.

# For example:
# unique_in_order('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
# unique_in_order('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
# unique_in_order([1,2,2,3,3])       == [1,2,3]
# REF: https://www.codewars.com/kata/54e6533c92449cc251001667
def unique_in_order1(iterable):
    return_list = [iterable[0]]
    i = 1
    j = 0
    length = len(iterable) - 1
    while i < length:
        if iterable[i] != return_list[j]:
            return_list.append(iterable[i])
            j += 1
        i += 1
    print(return_list)


def unique_in_order2(iterable):
    if not any(iterable):  # Check if iterable is empty
        return []

    return_list = [iterable[0]]
    i = 0  # You dont need this you can get last item in the list using [-1]
    for item in iterable:
        if item != return_list[i]:
            return_list.append(item)
            i += 1
    print(return_list)


def unique_in_order(iterable):
    if not any(iterable):  # Check if iterable is empty
        return []

    return_list = [iterable[0]]
    for item in iterable:
        if item != return_list[-1]:
            return_list.append(item)
    print(return_list)


unique_in_order('AAAABBBCCDAABBB')
unique_in_order('ABBCcAD')
unique_in_order([1, 2, 2, 3, 3])
