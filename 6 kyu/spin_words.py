# Write a function that takes in a string of one or more words,
# and returns the same string, but with all five or more letter words reversed
# (Just like the name of this Kata). Strings passed in will consist of only letters and spaces.
# Spaces will be included only when more than one word is present.
# Examples: spinWords( "Hey fellow warriors" ) => returns "Hey wollef sroirraw"
# spinWords( "This is a test") => returns "This is a test"
# spinWords( "This is another test" )=> returns "This is rehtona test"
# REF: https://www.codewars.com/kata/5264d2b162488dc400000001/train/python
def spin_words(sentence: str) -> str:
    # Your code goes here
    word_list = sentence.split(' ')
    for i in range(len(word_list)):
        if len(word_list[i]) > 4:
            word_list[i] = word_list[i][::-1]
    sentence = ' '.join(word_list)
    return sentence


spin_words("Hey fellow warriors")
spin_words("This is a test")
spin_words("This is another test")
