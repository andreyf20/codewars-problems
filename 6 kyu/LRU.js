/*
The LRU page replacement algorithm

The least recently used (LRU) page replacement algorithm works on the idea that pages 
that have been most heavily used in the past few instructions 
are most likely to be used heavily in the next few instructions too. 
If not all the slots in memory are occupied, the requested page is inserted in 
the first available slot when a page fault happens. If the memory is full and a page fault happens, 
the least recently used page in memory gets replaced by the requested page. 
To explain it in a clearer way: the least recently used page is the page 
that is currently in memory and has been referenced further in the past.

Your task is to implement this algorithm. 
The lru function will take two parameters as input: 
the number of maximum pages that can be kept in memory at the same time n 
and a reference list containing numbers. 
Every number represents a page request for a specific page 
(you can see this number as the unique ID of a page). 
The expected output is the status of the memory after the application of the algorithm. 
Note that when a page is inserted in the memory, 
it stays at the same position until it is removed from the memory by a page fault.

REF: https://www.codewars.com/kata/6329d94bf18e5d0e56bfca77/train/javascript
*/

/* 
EXAMPLE
    N = 3,
    REFERENCE LIST = [1, 2, 3, 4, 3, 2, 5],

  * 1 is read, page fault --> free memory available, memory = [1];
  * 2 is read, page fault --> free memory available, memory = [1, 2];
  * 3 is read, page fault --> free memory available, memory = [1, 2, 3];
  * 4 is read, page fault --> LRU = 1, memory = [4, 2, 3];
  * 3 is read, already in memory, nothing happens;
  * 2 is read, already in memory, nothing happens;
  * 5 is read, page fault --> LRU = 4, memory = [5, 2, 3].
*/

function initializeMem(n) {
    memory = [];
    for (let i = 0; i < n; i++) {
        memory.push(-1);
    }
    return memory;
}

function checkMemory(memory, page) {
    hit = false;
    memory.forEach(cachedPage => { 
        if (cachedPage === page) {
            hit = true;
            return;
        } 
    })
    return hit;
}

function updateMemory(memory, page, LRU) {
    for (let i = 0; i < memory.length; i++) {
        const cachedPage = memory[i];
        if (cachedPage === LRU) {
            memory[i] = page;
            return memory;
        }
    }
}

function updateLRUPage(orderStack, page, isNewPage = false) {
    if (isNewPage) {
        orderStack = orderStack.slice(1);
        orderStack.push(page);
        return orderStack; 
    }
    for (let i = 0; i < orderStack.length; i++) {
        const cachedPage = orderStack[i];
        if (cachedPage === page) {
            const tmp1 = orderStack.slice(0, i);
            const tmp2 = orderStack.slice(i + 1);
            orderStack = tmp1.concat(tmp2);
            orderStack.push(page);
            return orderStack;
        }
    }
}

function lru(n, referencesList) {
    memory = initializeMem(n);
    orderStack = []
    referencesList.forEach(page => {
        if (checkMemory(memory, page)) {
            // Mem Hit -> Update lru
            orderStack = updateLRUPage(orderStack, page);
        }
        else if (!checkMemory(memory, page) && (orderStack.length < n)) {
            // Add new memory and add lru
            orderStack.push(page);
            memory[orderStack.length - 1] = page;
        }
        else if (!checkMemory(memory, page) && (orderStack.length >= n)) {
            // Find lru and replace it 
            memory = updateMemory(memory, page, orderStack[0])
            orderStack = updateLRUPage(orderStack, page, true);
        }
    });
	return memory;
}

console.log(lru(3, [1, 2, 3, 4, 3, 2, 5]));
console.log(lru(5, []));
console.log(lru(4, [5, 4, 3, 2, 3, 5, 2, 6, 7, 8]));
console.log(lru(4, [1, 1, 1, 2, 2, 3]));
console.log(lru(1, [5, 4, 3, 3, 4, 10]));
console.log(lru(3, [1, 1, 1, 1, 1, 1, 1, 1]));
console.log(lru(5, [10, 9, 8, 7, 7, 8, 7, 6, 5, 4, 3, 4, 3, 4, 5, 6, 5]));