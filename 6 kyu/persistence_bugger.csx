/*
Write a function, persistence, that takes in a positive parameter num and returns its 
multiplicative persistence, which is the number of times you must multiply 
the digits in num until you reach a single digit.
For example:

persistence(39) == 3 // because 3*9 = 27, 2*7 = 14, 1*4=4
                     // and 4 has only one digit

persistence(999) == 4 // because 9*9*9 = 729, 7*2*9 = 126,
                      // 1*2*6 = 12, and finally 1*2 = 2

persistence(4) == 0 // because 4 is already a one-digit number

REF: https://www.codewars.com/kata/55bf01e5a717a0d57e0000ec/train/csharp 
*/
using System;
class Persist
{
    public static int Persistence(long n)
    {
        var lastNum = n % 10;
        n = n / 10;

        if (n == 0)
            return 0;

        var counter = 1;
        while (true)
        {
            while (n > 0)
            {
                lastNum = lastNum * (n % 10);
                n = n / 10;
            }
            if ((lastNum / 10) == 0)
                return counter;

            counter++;
            n = lastNum;
            lastNum = 1;
        }
    }
}

Console.WriteLine(Persist.Persistence(39));
Console.WriteLine(Persist.Persistence(999));
Console.WriteLine(Persist.Persistence(4));