// Your goal in this kata is to implement a difference function,
// which subtracts one list from another and returns the result.
// It should remove all values from list a, which are present in list b keeping their order.

// array_diff(vec![1,2], vec![1]) == vec![2]

// If a value is present in b, all of its occurrences must be removed from the other:
// array_diff(vec![1,2,2,2,3], vec![2]) == vec![1,3]
// REF: https://www.codewars.com/kata/523f5d21c841566fde000009

// Brute force solution
// fn array_diff<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
//     let mut result = Vec::new();
//     for i in a {
//         let mut insert_item = true;
//         for j in &b {
//             if &i == j {
//                 insert_item = false;
//                 break;
//             }
//         }
//         if insert_item {
//             result.push(i);
//         }
//     }
//     result
// }

// Solution 2 cleaner
// Could also use a.retain(|x| !b.contains(x)); or filter instead of retain
fn array_diff<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
    let mut result = Vec::new();
    for i in a {
        if !b.contains(&i) {
            result.push(i);
        }
    }
    result
}

fn main() {
    println!("{:?}", array_diff(vec![1, 2], vec![1])); // vec![2]
    println!("{:?}", array_diff(vec![1, 2, 2], vec![1])); // vec![2,2]
    println!("{:?}", array_diff(vec![1, 2, 2], vec![2])); // vec![1]
}
