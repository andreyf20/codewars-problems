# Description:
# There was a test in your class and you passed it. Congratulations!
# But you're an ambitious person. You want to know if you're better than the average student in your class.
# You receive an array with your peers' test scores. Now calculate the average and compare your score!
# Return True if you're better, else False!
# Note:
# Your points are not included in the array of your class's points. 
# For calculating the average point you may add your point to the given array!
# REF: https://www.codewars.com/kata/5601409514fc93442500010b/elixir

defmodule Detector do
  defp get_average_aux([h | t], accum) do
    if t == [] do
      accum + h
    else
      get_average_aux(t, accum + h)
    end
  end

  defp get_average(class_points),
    do: get_average_aux(class_points, 0) / length(class_points)

  def better_than_average(class_points, your_points) do
    get_average([your_points | class_points]) < your_points
  end
end

result = Detector.better_than_average([2, 3], 5)
IO.puts(result)
result = Detector.better_than_average([100, 40, 34, 57, 29, 72, 57, 88], 75)
IO.puts(result)
result = Detector.better_than_average([12, 23, 34, 45, 56, 67, 78, 89, 90], 9)
IO.puts(result)
