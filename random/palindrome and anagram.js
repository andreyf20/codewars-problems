function anagram(word1, word2){
    if(word1.length !== word2.length) return false
    
    for (let i = 0; i < word1.length; i++) {
        for (let j = 0; j < word2.length; j++) {
            if(word1[i] === word2[j]) {
                word2.replace(word1[i], '')
                break
            }
            else if(j + 1 === word2.length) return false
        }
    }
    return true
}

function anagram2(word1, word2){
    if(word1.length !== word2.length) return false

    let sum1 = 0
    let sum2 = 0
    for (let i = 0; i < word1.length; i++) {
        const char1 = word1.charCodeAt(i);
        const char2 = word2.charCodeAt(i);
        sum1 += char1
        sum2 += char2
    }

    return sum1 === sum2
}

function anagram3(word1, word2){
    if(word1.length !== word2.length){
        console.log(false)
        return false
    }
    word1 = word1.split('').sort().join('')
    word2 = word2.split('').sort().join('')

    return word1 === word2
}

function palindrome(word){
    let i = 0;
    let j = word.length - 1;
    while (i < j) {
        if (word[i] !== word[j]) return false
        i++
        j--
    }
    return true
}

function palindrome2(word){
    return word === word.split('').reverse().join('')
}

console.log(palindrome(`detartrated`))
console.log(anagram3(`adobe`, `abode`))