# Write a function named sumDigits which takes a number as input
# and returns the sum of the absolute value of each of the number's decimal digits.
# For example: (Input --> Output)
# 10 --> 1
# 99 --> 18
# -32 --> 5
# Let's assume that all numbers in the input will be integer values.
# REF: https://www.codewars.com/kata/52f3149496de55aded000410/train/

defmodule Kata do
  defp sum_digits_aux(number, acc) do
    if number == 0 do
      acc
    else
      dig = rem(number, 10) |> abs()
      trunc(number / 10) |> sum_digits_aux(acc + dig)
    end
  end

  def sum_digits(number) do
    sum_digits_aux(number, 0)
  end
end

result = Kata.sum_digits(10)
IO.puts(result)
result = Kata.sum_digits(99)
IO.puts(result)
result = Kata.sum_digits(-32)
IO.puts(result)
