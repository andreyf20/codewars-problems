/*
In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.

Example:

Kata.HighAndLow("1 2 3 4 5");  // return "5 1"
Kata.HighAndLow("1 2 -3 4 5"); // return "5 -3"
Kata.HighAndLow("1 9 3 4 -5"); // return "9 -5"
Notes:

All numbers are valid Int32, no need to validate them.
There will always be at least one number in the input string.
Output string must be two numbers separated by a single space, and highest number is first.
REF: https://www.codewars.com/kata/554b4ac871d6813a03000035/train/csharp
*/

using System;

public static class Kata
{
    public static string HighAndLow(string numbers)
    {
        String[] numberList = numbers.Split(' ');
        int min, max;
        min = max = Int32.Parse(numberList[0]);

        if (numberList.Length > 1)
        {
            foreach (var number in numberList)
            {
                int num = Int32.Parse(number);
                if (max < num)
                {
                    max = num;
                    continue;
                }
                if (min > num)
                {
                    min = num;
                }
            }

        }

        return $"{max} {min}";
    }
}


Console.WriteLine(Kata.HighAndLow("1 2 3 4 5"));
Console.WriteLine(Kata.HighAndLow("1 2 -3 4 5"));
Console.WriteLine(Kata.HighAndLow("1 9 3 4 -5"));