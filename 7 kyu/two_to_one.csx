/*
Take 2 strings s1 and s2 including only letters from ato z. Return a new sorted string, the longest possible, containing distinct letters,

each taken only once - coming from s1 or s2.
Examples:
a = "xyaabbbccccdefww"
b = "xxxxyyyyabklmopq"
longest(a, b) -> "abcdefklmopqwxy"

a = "abcdefghijklmnopqrstuvwxyz"
longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"
REF: https://www.codewars.com/kata/5656b6906de340bd1b0000ac/train/csharp
*/

public class TwoToOne
{
    private static bool charInString(char c, string str)
    {
        foreach (char character in str)
        {
            if (character == c)
                return true;
        }
        return false;
    }

    public static string Longest(string s1, string s2)
    {
        var unique = new StringBuilder();

        foreach (var character in s1)
        {
            if (!charInString(character, unique.ToString()))
            {
                unique.Append(character);
            }
        }

        foreach (var character in s2)
        {
            if (!charInString(character, unique.ToString()))
            {
                unique.Append(character);
            }
        }

        var charArray = unique.ToString().ToCharArray();
        Array.Sort(charArray);
        return new string(charArray);
    }
}

Console.WriteLine(TwoToOne.Longest("xyaabbbccccdefww", "xxxxyyyyabklmopq"));
Console.WriteLine(TwoToOne.Longest("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyz"));