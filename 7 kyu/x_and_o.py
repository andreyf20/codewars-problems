# Check to see if a string has the same amount of 'x's and 'o's.
# The method must return a boolean and be case insensitive. The string can contain any char.

# Examples input/output:
# XO("ooxx") => true
# XO("xooxx") => false
# XO("ooxXm") => true
# XO("zpzpzpp") => true // when no 'x' and 'o' is present should return true
# XO("zzoo") => false
# REF: https://www.codewars.com/kata/55908aad6620c066bc00002a/train/python

def xo(s: str):
    s = s.lower()
    xs = 0
    os = 0
    for item in s:
        if item == 'x':
            xs += 1
        elif item == 'o':
            os += 1
    print(xs == os)
    return xs == os


xo("ooxx")
xo("xooxx")
xo("ooxXm")
xo("zpzpzpp")
xo("zzoo")
