/*
Given two integers a and b, which can be positive or negative, find the sum of all the numbers between including them too and return it. If the two numbers are equal return a or b.

Note: a and b are not ordered!

Examples
GetSum(1, 0) == 1   // 1 + 0 = 1
GetSum(1, 2) == 3   // 1 + 2 = 3
GetSum(0, 1) == 1   // 0 + 1 = 1
GetSum(1, 1) == 1   // 1 Since both are same
GetSum(-1, 0) == -1 // -1 + 0 = -1
GetSum(-1, 2) == 2  // -1 + 0 + 1 + 2 = 2
*/

using System;
public class Sum
{
    private static int GetSumOrdered(int a, int b)
    {
        int sum = 0;
        while (a <= b)
        {
            sum += a;
            a++;
        }
        return sum;
    }
    public static int GetSum(int a, int b)
    {
        if (a == b)
            return a;
        else if (a < b)
            return GetSumOrdered(a, b);
        else
            return GetSumOrdered(b, a);
    }
}

Console.WriteLine(Sum.GetSum(1, 0));
Console.WriteLine(Sum.GetSum(1, 2));
Console.WriteLine(Sum.GetSum(0, 1));
Console.WriteLine(Sum.GetSum(1, 1));
Console.WriteLine(Sum.GetSum(-1, 0));
Console.WriteLine(Sum.GetSum(-1, 2));

