/*
Given the triangle of consecutive odd numbers:

             1
          3     5
       7     9    11
   13    15    17    19
21    23    25    27    29
...
Calculate the row sums of this triangle from the row index (starting at index 1) e.g.:

rowSumOddNumbers(1); // 1
rowSumOddNumbers(2); // 3 + 5 = 8
REF: https://www.codewars.com/kata/55fd2d567d94ac3bc9000064/train/csharp
*/

using System;
public static class Kata
{
    public static long rowSumOddNumbers(long n)
    {
        if (n == 1)
            return 1;

        long rowFirstNum = 3;
        long counter = 2;

        while (counter < n)
        {
            rowFirstNum += (2 * counter);
            counter++;
        }

        long sum = rowFirstNum;

        while (n > 1)
        {
            rowFirstNum += 2;
            sum += rowFirstNum;
            n--;
        }

        return sum;
    }
}

Console.WriteLine(Kata.rowSumOddNumbers(1));
Console.WriteLine(Kata.rowSumOddNumbers(2));
Console.WriteLine(Kata.rowSumOddNumbers(3));
Console.WriteLine(Kata.rowSumOddNumbers(4));
Console.WriteLine(Kata.rowSumOddNumbers(42));