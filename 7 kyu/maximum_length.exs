# You are given two arrays a1 and a2 of strings.
# Each string is composed with letters from a to z.
# Let x be any string in the first array and y be any string in the second array.
# Find max(abs(length(x) − length(y)))
# If a1 and/or a2 are empty return -1 in each language except in Haskell (F#)
# where you will return Nothing (None).
# Example:
# a1 = ["hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"]
# a2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
# mxdiflg(a1, a2) --> 13
# Bash note:
#     input : 2 strings with substrings separated by ,
#     output: number as a string
# REF: https://www.codewars.com/kata/5663f5305102699bad000056/

defmodule MaxDiff do
  defp find_max_min([h | t], current_max, current_min) do
    current_len = String.length(h)
    current_max = if current_len > current_max, do: current_len, else: current_max

    current_min =
      if current_len < current_min || current_min == -1, do: current_len, else: current_min

    if t == [] do
      {current_max, current_min}
    else
      find_max_min(t, current_max, current_min)
    end
  end

  def mxdiflg(a1, a2) do
    if a1 == [] || a2 == [] do
      -1
    else
      {a1_max, a1_min} = find_max_min(a1, -1, -1)
      {a2_max, a2_min} = find_max_min(a2, -1, -1)
      restult1 = (a1_max - a2_min) |> abs()
      result2 = (a2_max - a1_min) |> abs()
      if restult1 > result2, do: restult1, else: result2
    end
  end
end

a1 = [
  "hoqq",
  "bbllkw",
  "oox",
  "ejjuyyy",
  "plmiis",
  "xxxzgpsssa",
  "xxwwkktt",
  "znnnnfqknaz",
  "qqquuhii",
  "dvvvwz"
]

a2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
result = MaxDiff.mxdiflg(a1, a2)
IO.puts("result = #{result}")

a1 = []
a2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
result = MaxDiff.mxdiflg(a1, a2)
IO.puts("result = #{result}")

a1 = []
a2 = []
result = MaxDiff.mxdiflg(a1, a2)
IO.puts("result = #{result}")

a1 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"]
a2 = []
result = MaxDiff.mxdiflg(a1, a2)
IO.puts("result = #{result}")
