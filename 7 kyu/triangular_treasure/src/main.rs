// Triangular numbers are so called because of the equilateral
// triangular shape that they occupy when laid out as dots. i.e.

// 1st (1)   2nd (3)    3rd (6)
// *          **        ***
//            *         **
//                      *

// You need to return the nth triangular number. You should return 0 for out of range values:

// For example: (Input --> Output)

// 0 --> 0
// 2 --> 3
// 3 --> 6
// -10 --> 0
// REF: https://www.codewars.com/kata/525e5a1cb735154b320002c8

fn triangular(n: i32) -> i32 {
    if n <= 0 {
        // Edge case anything below or equal to 0 is 0
        return 0;
    }
    let mut result = 0;
    for i in 1..n + 1 {
        result += i;
    }
    result
}

// fn triangular(n: i32) -> i32 {
//     if n <= 0 {
//         // Edge case anything below or equal to 0 is 0
//         return 0;
//     }
//     let mut counter = n;
//     let mut result = 0;
//     while counter > 0 {
//         result += counter;
//         counter -= 1;
//     }
//     result
// }

fn main() {
    println!("{}", triangular(2)); // 3
    println!("{}", triangular(4)); // 10
    println!("{}", triangular(9)); // 45
    println!("{}", triangular(-9)); // 0
}
