// Complete the solution so that it returns true if the first argument(string)
// passed in ends with the 2nd argument (also a string).

// Examples:

// solution('abc', 'bc') // returns true
// solution('abc', 'd') // returns false

// Cheating?
// fn solution2(word: &str, ending: &str) -> bool {
//     word.ends_with(ending) // Using the library function
// }

fn solution(word: &str, ending: &str) -> bool {
    if word.len() < ending.len() {
        // Prevent overflow because ending is bigger than word
        return false;
    }
    let word_slice = &word[word.len() - ending.len()..];
    word_slice == ending
}

fn main() {
    println!("{}", solution("abc", "bc"));
    println!("{}", solution("abc", "d"));
    println!("{}", solution("strawberry", "banana"));
}
