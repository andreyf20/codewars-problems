// Implement a function that accepts 3 integer values a, b, c.
// The function should return true if a triangle can be built with the sides 
// of given length and false in any other case.
// (In this case, all triangles must have surface greater than 0 to be accepted).
// REF: https://www.codewars.com/kata/56606694ec01347ce800001b

fn is_triangle(a: i64, b: i64, c: i64) -> bool {
    // https://www.wikihow.com/Determine-if-Three-Side-Lengths-Are-a-Triangle
    if a + b > c && a + c > b && b + c > a {
        return true;
    }
    false
}

fn main() {
    println!("{}", is_triangle(1, 2, 2));
    println!("{}", is_triangle(7, 2, 2));
    println!("{}", is_triangle(1, 2, 3));
    println!("{}", is_triangle(1, 3, 2));
    println!("{}", is_triangle(3, 1, 2));
    println!("{}", is_triangle(5, 1, 2));
    println!("{}", is_triangle(1, 2, 5));
    println!("{}", is_triangle(2, 5, 1));
    println!("{}", is_triangle(4, 2, 3));
    println!("{}", is_triangle(5, 1, 5));
    println!("{}", is_triangle(2, 2, 2));
    println!("{}", is_triangle(-1, 2, 3));
    println!("{}", is_triangle(1, -2, 3));
    println!("{}", is_triangle(1, 2, -3));
    println!("{}", is_triangle(0, 2, 3));
}
