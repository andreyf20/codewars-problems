// In this kata you will be given a sequence of the dimensions of rectangles
// ( sequence with width and length ) and circles ( radius - just a number ).
// Your task is to return a new sequence of dimensions, sorted ascending by area.
// REF: https://www.codewars.com/kata/5a1ebc2480171f29cf0000e5
use either::Either;

fn get_rectangle_area(w: f64, h: f64) -> f64 {
    w * h
}

fn get_circle_area(r: f64) -> f64 {
    std::f64::consts::PI * r * r
}

fn get_sorted_areas(mutable_areas: &mut Vec<f64>, new_area: f64) -> usize {
    for (i, area) in mutable_areas.iter().enumerate() {
        if &new_area < area {
            mutable_areas.insert(i, new_area);
            return i;
        }
    }
    mutable_areas.push(new_area);
    return usize::MAX;
}

fn sort_by_area(seq: &[Either<(f64, f64), f64>]) -> Vec<Either<(f64, f64), f64>> {
    let mut result = Vec::new();
    let mut areas = Vec::new();
    for i in seq {
        let mut area = 0.0;
        if i.is_left() {
            // Rectangle case
            let (w, h) = i.left().unwrap();
            area = get_rectangle_area(w, h);
        } else if i.is_right() {
            // Circle case
            let r = i.right().unwrap();
            area = get_circle_area(r);
        }
        let index = get_sorted_areas(&mut areas, area);
        if index < usize::MAX {
            result.insert(index, i.clone());
        } else {
            result.push(i.clone());
        }
    }
    result
}

fn main() {
    let seq = &[
        Either::Left((4.23, 6.43)),
        Either::Right(1.23),
        Either::Right(3.444),
        Either::Left((1.342, 3.212)),
    ]; // ( rectangle, circle, circle, rectangle )
    println!("{:?}", sort_by_area(seq)); // [ Either::Right(1.23), Either::Left((1.342, 3.212)), Either::Right(3.444), Either::Left((4.23, 6.43)) ]
}
